package main

import (
	"fmt"
	"gamex/antnet"
)

type MyMsgHandler struct {
}

func (r *MyMsgHandler) OnNewMsgQue(msgque antnet.IMsgQue) bool { return true }
func (r *MyMsgHandler) OnDelMsgQue(m antnet.IMsgQue) {
}
func (r *MyMsgHandler) OnProcessMsg(m antnet.IMsgQue, msg *antnet.Message) bool {
	fmt.Printf("recv msg send back\n")
	m.Send(msg)
	return true
}
func (r *MyMsgHandler) OnConnectComplete(msgque antnet.IMsgQue, ok bool) bool { return true }

func cleanServerInfo() {
}
func main() {
	wsaddr := "ws://:6001"
	fmt.Printf("game start ws:%v\n", wsaddr)

	msgHandler := &MyMsgHandler{}
	antnet.StartServer(wsaddr, msgHandler)
	antnet.WaitForSystemExit(cleanServerInfo)
}
