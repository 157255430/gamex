module gamex

go 1.14

require (
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/websocket v1.4.2
	github.com/orcaman/concurrent-map v0.0.0-20190826125027-8c72a8bb44f6 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	go.uber.org/zap v1.14.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
)

//require (
//	github.com/go-redis/redis v6.15.7+incompatible
//	github.com/golang/protobuf v1.3.4
//	github.com/gorilla/websocket v1.4.1
//	github.com/huandu/xstrings v1.3.0
//	github.com/jinzhu/gorm v1.9.12
//	github.com/json-iterator/go v1.1.9
//	github.com/onsi/ginkgo v1.12.0 // indirect
//	github.com/onsi/gomega v1.9.0 // indirect
//	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
//	github.com/pquerna/ffjson v0.0.0-20190930134022-aa0246cd15f7
//)
